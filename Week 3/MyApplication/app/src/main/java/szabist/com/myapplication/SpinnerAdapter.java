package szabist.com.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by salmanjawed on 01/10/2017.
 */

public class SpinnerAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<ModelSpinner> genderList;
    LayoutInflater mInflater;

    public SpinnerAdapter(@NonNull Context context, @NonNull ArrayList<ModelSpinner> objects) {

        this.mContext = context;
        this.genderList = objects;
        mInflater = LayoutInflater.from(mContext);

    }

    @Nullable
    @Override
    public ModelSpinner getItem(int position) {
        return genderList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public int getCount() {
        return genderList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View mView = mInflater.inflate(R.layout.row_spinner, null);

        TextView tvText = (TextView) mView.findViewById(R.id.tv_text_spinner);

        tvText.setText(genderList.get(position).getGenderText());

        return mView;
    }
}
