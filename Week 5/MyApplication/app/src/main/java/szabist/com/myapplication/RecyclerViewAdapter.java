package szabist.com.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by salmanjawed on 07/10/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    ArrayList<ModelRecyclerView> mList;
    Context mContext;

    public RecyclerViewAdapter(ArrayList<ModelRecyclerView> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public RecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_recycler_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.MyViewHolder holder, int position) {
        ModelRecyclerView movie = mList.get(position);
        holder.title.setText(movie.getMovieTitle());
        holder.genre.setText(movie.getMovieGenre());
        holder.description.setText(movie.getMovieDescription());

        Picasso.with(mContext).load(movie.getMovieImage()).into(holder.ivImg);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description, genre;
        public ImageView ivImg;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tv_movie_title);
            ivImg = (ImageView) view.findViewById(R.id.iv_img);
            genre = (TextView) view.findViewById(R.id.tv_movie_genre);
            description = (TextView) view.findViewById(R.id.tv_movie_description);
        }
    }

}
