package szabist.com.myapplication;

/**
 * Created by salmanjawed on 07/10/2017.
 */

public class ModelViewPager {

    private String title;

    private String imageUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
