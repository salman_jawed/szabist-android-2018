package szabist.com.myapplication;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by salmanjawed on 07/10/2017.
 */

public class CustomPagerAdapter extends PagerAdapter {


    Context mContext;
    ArrayList<ModelViewPager> mList;

    public CustomPagerAdapter(Context context, ArrayList<ModelViewPager> items) {
        this.mContext = context;
        this.mList = items;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mList.get(position).getTitle();
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        ModelViewPager mModel = mList.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_view_pager, collection, false);

        TextView tvTitle = (TextView) layout.findViewById(R.id.tv_title);
        tvTitle.setText(mModel.getTitle());

        ImageView iv = (ImageView) layout.findViewById(R.id.iv_image);
        Picasso.with(mContext).load(mModel.getImageUrl()).into(iv);

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

}
