package szabist.com.myapplication;

/**
 * Created by Test on 10/7/2017.
 */

public class ModelCountry {

    public String getCountryId() {
        return CountryId;
    }

    public void setCountryId(String countryId) {
        CountryId = countryId;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    private String CountryId, CountryName;

}
